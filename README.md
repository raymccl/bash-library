# Bash Scripts

A repository of helpful bash scripts for use on all my systems.

## Setup

In your .bashrc or .zshrc file, add the following at the end:

source "[Path to your alias.sh file]"

This will load all the alias commands found in the alias.sh file for use.
