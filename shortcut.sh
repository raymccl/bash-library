#!/bin/sh
# Get the current directory and load the config
if ! [ -x "$(command -v dialog)" ]; then
        apt-get install dialog;
fi

HEIGHT=22
WIDTH=40
CHOICE_HEIGHT=20
TITLE="Open project"
MENU="Choose project to open:"
OPTIONS=(
        1 "CPP"
        2 "Web"
        3 "Backend"
        4 "Automation"
        5 "Game Dev"
	6 "Bash"
)
CHOICE=$(dialog --clear \
        --no-shadow \
                        --title "$TITLE" \
                        --menu "$MENU" \
                        $HEIGHT $WIDTH $CHOICE_HEIGHT \
                        "${OPTIONS[@]}" \
                        2>&1 >/dev/tty)
clear;
# You can configure this however you see fit, just don't commit it back please :D
case $CHOICE in
        1) cd "/home/ray/cpp/";;
	2) cd "/home/ray/www/";;
	3) cd "/home/ray/backend/";;
	4) cd "/home/ray/automation/";;
	5) cd "/home/ray/games/";;
	6) cd "/home/ray/bash/";;
esac
